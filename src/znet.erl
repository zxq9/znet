%%% @doc
%%% znet: Network convenience functions for Erlang
%%% @end

-module(znet).
-vsn("0.1.0").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-export([peername/1, host_string/1, ntoa/1, default_iface_info/0]).


-spec peername(Socket) -> Result
    when Socket :: gen_tcp:socket(),
         Result :: {ok, zx:host()}
                 | {error, inet:posix()}.
%% @doc
%% Returns the IPv4 or IPv6 peer-side address of a connected socket, translating the
%% address to IPv4 in the case that it is a translated IPv4 -> IPv6 address.

peername(Socket) ->
    case inet:peername(Socket) of
        {ok, {{0, 0, 0, 0, 0, 65535, X, Y}, Port}} ->
            <<A:8, B:8, C:8, D:8>> = <<X:16, Y:16>>,
            {ok, {{A, B, C, D}, Port}};
        Other ->
            Other
    end.


-spec host_string(Host) -> string()
    when Host :: {inet:hostname() | inet:ip_address(), inet:port_number()}.
%% @doc
%% Returns the string formatted "Hostname:Port" combination.
%% Examples:
%%   `host_string({"foo.com", 12345}) -> "foo.com:12345"'
%%   `host_string({{1,2,3,4}, 12345}) -> "1.2.3.4:12345"'
%%   `host_string({localhost, 12345}) -> "localhost:12345"'

host_string({Address, Port}) ->
    lists:flatten([ntoa(Address), ":", integer_to_list(Port)]).


-spec ntoa(Address) -> string()
    when Address :: inet:hostname() | inet:ip_address().
%% @doc
%% Similar to inet:ntoa, but accepts atoms and strings as valid addresses.

ntoa(Address) when is_list(Address)  -> Address;
ntoa(Address) when is_tuple(Address) -> inet:ntoa(Address);
ntoa(Address) when is_atom(Address)  -> atom_to_list(Address).


-spec default_iface_info() -> Result
    when Result    :: {ok, {GatewayIP, IFace, LocalIP}}
                    | {error, unsupported},
         GatewayIP :: inet:ip_address(),
         IFace     :: string(),
         LocalIP   :: inet:ip_address().

default_iface_info() -> default_iface_info(os:type()).

default_iface_info({unix, linux}) ->
    Out = os:cmd("ip route get 8.8.8.8"),
    case string:lexemes(Out, " \n") of
        [_, "via", Gateway, "dev", IFace, "src", Local | _] ->
            {ok, GatewayIP} = inet:parse_address(Gateway),
            {ok, LocalIP} = inet:parse_address(Local),
            {ok, {GatewayIP, IFace, LocalIP}};
        _ ->
            {error, unsupported}
    end.
